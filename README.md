# Messsenger

### Stack information:
* Host : Digital Ocean : 1 vCPU, 512 MB RAM.
* Operating System: Ubuntu 16.04.
* Server : Apache.
* Database : Mysql + Redis
* Back-end language: PHP
* Back-end framework : Laravel
* Front-end: 
    * VueJs (Very similar to React)
    * Axios
    * Bootstrap 3
    * Webpack for assets compiling (Laravel mix)
    * Icons: Font awesome.
    
### Directory structure:
Laravel directory structure could feel a bit bloated compared to Django.
* Database migrations (If you want to take a look at the database strucure): `/database/migrations`
* Routes : `/routes/web.php`
* Controllers: `app/Http/Controllers`
* Models : `/app`
* Factories: `/app/Factories`
* Repositories: `/app/Repositories`
* Events : Events and listeners are registered in `/app/Providers/EventServiceProvider.php`
* ConversationCache : `/app/CacheSystem/ConversationCache.php`
* Dependency injection (binding some singletons to the service container) : `app/Providers/AppServiceProvider.php`
* Frontend : 
    * Views: `/resources/views`
    * JS : `/resources/assets/js`
    * CSS : `/resources/assets/sass/custom.scss` (not much to see)

I would suggest to begin reading the code from the routes file. You can then read the mapped controllers, the controller dependencies and their dependencies ...

### Implemented additional features:
* Auto complete functionality when typing recipient.
* Recent contacts are cached in a sorted list (Redis).
* Multiple attachments are allowed.

### Solved edge cases: 
* Malicious attempts to access or post attachments or messages where the user is not a part of the conversation.
* The conversation is only hidden from the inbox when one user has deleted it. The counter part would still be able to see it.
* All related rows and attachments are deleted when a conversation is deleted by both parts.
    
### Notes: 
* The application should satisfy all requirements for a junior developer and even some of the senior requirements (Attaching files, caching system, acceptable response time).
* No tests are written (I don't usually write tests to be honest).
* The application is not fully commented (But still very readable).
* Only complicated and ineffective queries are written in raw SQL otherwise an ORM is used.
* Must of the front-end layout is provided by Laravel
* Login/Register functionality is also provided by Laravel, but it requires som configuration. It has been also slightly tweaked to allow profile images.
* Password reset feature doesn't work (I have not configured a mail server).
* The CacheSystem is relatively basic and could be improved.
* The application has not been tested with large data sets and many concurrent users.
* The front-end needs some more refactoring in some areas.

### Things i wanted to implement (If I had more time):
* Real-time notifications with socket.io ,Redis and Laravel echo.
* Profile image resizing job (with appropriate queue system).
* Is Read and is seen functionality.
