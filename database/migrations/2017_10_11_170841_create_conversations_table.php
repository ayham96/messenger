<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('initiator_id');
            $table->unsignedInteger('counter_part_id');
            $table->unsignedInteger('last_message_id')->nullable();

            $table->foreign('last_message_id')->references('id')->on('messages')->onDelete('set null');
            $table->foreign('initiator_id')->references('id')->on('users');
            $table->foreign('counter_part_id')->refences('id')->on('users');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
