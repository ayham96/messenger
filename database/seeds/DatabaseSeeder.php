<?php

use App\Factories\ConversationFactory;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run(\Faker\Generator $faker)
    {
        // $this->call(UsersTableSeeder::class);
        for ($i=0;$i < 10000; $i++) {
            $sender = rand(1,20);
            $recipient = rand(1,19);
            if ($sender === $recipient) {
                $recipient++;
            }
            ConversationFactory::make()
                ->setInitiator($sender)
                ->setCounterPart($recipient)
                ->setMessage($faker->sentence())
                ->save();
        }
    }
}
