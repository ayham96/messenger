<?php

Route::get('/', 'LandingPageController');

Auth::routes();

Route::get('/inbox', 'InboxController');

Route::resource('/archive', 'ArchiveController', ['only' => [
    'index', 'store'
]]);

Route::resource('/conversations','ConversationsController', ['only'=> [
    'show', 'destroy'
]]);

Route::resource('/messages','MessagesController',['only'=> [
    'create', 'store'
]]);


Route::get('/contacts', 'ContactsController');

Route::get('/attachments/{attachment}', 'AttachmentsController');

Route::get('/search/contacts' , 'ContactsSearchController');

