export default class ContactsSearchAPI
{
    static getSuggestions(query, successCallback) {
        axios.get('/search/contacts?query='+query).then(
            successCallback
        ).catch(function (error) {
            console.log("error while searching for recipients");
            console.log(error);
        });
    }
}
