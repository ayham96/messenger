export default class RecipientSuggestionsAPI
{
    static delete(id, successCallback) {
        axios.delete('/conversations/'+id)
            .then(successCallback)
            .catch(function (error) {
                console.log("error while deleting conversation");
                console.log(error)
            });
    }

    static archive(id, successCallback) {
        const data = {conversation_id: id};
        axios.post('/archive', data)
            .then(successCallback)
            .catch(function (error) {
                console.log("error while storing conversation in archive");
                console.log(error)
            });
    }
}
