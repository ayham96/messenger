@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3>Inbox:</h3>
                @if (empty($conversations))
                    No messages to show.
                @endif
                <inbox-list :conversations-prop='@json($conversations)'
                            :user='@json(auth()->user())'
                            :page="{{$page}}">
                </inbox-list>
            </div>
        </div>
    </div>
@endsection
