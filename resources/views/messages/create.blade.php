@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if (!empty($errors->all()))
                    <div class="alert alert-danger">An error has occured while sending your message. Please check that the provided data is valid.</div>
                @endif

                @if ($recipient !== null)
                    <h3>New message to {{$recipient->name}}:</h3>
                    <message-form :recipient-id="{{$recipient->id}}">
                    </message-form>
                @else
                    <h3>New message:</h3>
                    <message-form>
                    </message-form>
                @endif
            </div>
        </div>
    </div>
@endsection
