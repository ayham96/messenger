@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3>Conversation with {{$counterPart->name}}:</h3>
                <message-list :messages='@json($messages->all())'
                              :user='@json(auth()->user())'
                              :counter-part='@json($counterPart)'>
                </message-list>
            </div>

        </div>
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3>Write a reply:</h3>
                <message-form :conversation-id="{{$conversationId}}"
                              :recipient-id="{{$counterPart->id}}">
                </message-form>
            </div>
        </div>
    </div>
@endsection
