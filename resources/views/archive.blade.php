@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <h3>Archive:</h3>
                <inbox-list :conversations-prop='@json($conversations)'
                            :user='@json(auth()->user())'
                            :archived='true'
                            :page="{{$page}}">
                </inbox-list>
            </div>
        </div>
    </div>
@endsection
