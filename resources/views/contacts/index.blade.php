@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                @if($recentContacts->isNotEmpty())
                <h3>Recent contacts:</h3>
                    <ul class="media-list">
                    @foreach ($recentContacts->all() as $contact)
                        <li class="media">
                            <a href="/messages/create?recipient_id={{$contact->id}}" style="text-decoration:none">
                                <div class="media-left">
                                    <img class="media-object profile-image-medium" src="/storage/{{$contact->avatar_path}}">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{$contact->name}}</h4>
                                    <p>Email: {{$contact->email}}</p>
                                </div>
                            </a>
                            <hr>
                        </li>
                    @endforeach
                    </ul>
                    <hr>
                @endif

                <h3>All contacts:</h3>
                <ul class="media-list">
                    @foreach ($contacts->all() as $contact)
                        <li class="media">
                            <a href="/messages/create?recipient_id={{$contact->id}}" style="text-decoration:none">
                                <div class="media-left">
                                    <img class="media-object profile-image-medium" src="/storage/{{$contact->avatar_path}}">
                                </div>
                                <div class="media-body">
                                    <h4 class="media-heading">{{$contact->name}}</h4>
                                    <p>Email: {{$contact->email}}</p>
                                </div>
                            </a>
                            <hr>
                        </li>
                    @endforeach
                </ul>

                {{$contacts->links()}}
            </div>
        </div>
    </div>
@endsection
