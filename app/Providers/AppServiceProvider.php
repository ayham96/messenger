<?php

namespace App\Providers;

use App\CacheSystem\ConversationCache;
use App\Managers\ConversationManager;
use App\Repositories\ConversationRepository;
use App\Repositories\RecentContactsRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton('App\CacheSystem\ConversationCache', function ($app) {
            return new ConversationCache();
        });

        $this->app->singleton('App\Managers\ConversationManager', function ($app) {
             $conversationRepository = $app->make(ConversationRepository::class);
             return new ConversationManager($conversationRepository);
        });

        $this->app->singleton('App\Repositories\RecentContactsRepository', function ($app) {
            return new RecentContactsRepository(auth()->id());
        });
    }
}
