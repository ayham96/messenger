<?php
namespace App\Managers;


use App\Conversation;
use App\Events\ConversationArchived;
use App\Events\ConversationTrashed;
use App\Repositories\ConversationRepository;
use Carbon\Carbon;
use Illuminate\Support\Facades\DB;

class ConversationManager {
    private $conversationRepository;

    public function __construct(ConversationRepository $conversationRepository)
    {
        $this->conversationRepository = $conversationRepository;
    }


    public function getInbox($page = 1, $user = null) {
        $user = $user ?? auth()->user();
        return $this->conversationRepository->inbox($user->id, $page);
    }

    public function getArchived($user = null, $page = 1) {
        $user = $user ?? auth()->user();
        return $this->conversationRepository->archive($user->id, $page);
    }

    public function updateLastMessage($conversationId, $messageId) {
        DB::table('conversations')
            ->where('id',$conversationId)
            ->update([
                'last_message_id'=>$messageId,
                'updated_at' => Carbon::now(),
            ]);
    }

    public function moveToInbox($userId, $conversationId) {
        $this->updateState($userId, $conversationId, 'inbox');
    }

    public function archive($userId, $conversationId) {
        $this->updateState($userId, $conversationId, 'archived');
        event(new ConversationArchived($userId, $conversationId));
    }

    public function isPart($userId, $conversationId) {
        $pivotCount = DB::table('user_conversation_pivot')
            ->where([
                'user_id'=> $userId,
                'conversation_id' => $conversationId
            ])->count();
        return ($pivotCount === 1);
    }

    public function getCounterPart($conversationId, $userId) {
        $bindings = [
            'conversation_id' => $conversationId,
            'user_id' => $userId
        ];
        $results =  DB::select(DB::raw("
            SELECT id, name, avatar_path FROM users
            JOIN user_conversation_pivot AS pivot
            ON (pivot.conversation_id = :conversation_id AND users.id = pivot.user_id  AND pivot.user_id <> :user_id)
        "), $bindings);
        return $results[0] ?? null;
    }

    public function getMessages($conversationId) {
        return $this->conversationRepository->messagesFor($conversationId);
    }

    /*
     * This method changes the state of the conversation for the specified user.
     * If the conversation is deleted by both then the conversation
     * and all related models would by deleted from database.
     * Deleting related attachments is handled by ConversationDeletedListener.
     */
    public function delete($userId, $conversationId) {
        $this->updateState($userId, $conversationId, 'deleted');
        event(new ConversationTrashed($userId, $conversationId));
        if ($this->isDeletedByBoth($conversationId)) {
            Conversation::destroy($conversationId);
        }
    }

    /**
     * return true if both parts of the conversation has deleted it.
     */
    private function isDeletedByBoth($conversationId) {
        $deletedCount = DB::table('user_conversation_pivot')
            ->where([
                'conversation_id'=> $conversationId,
                'state' => 'deleted'
            ])
            ->count();
        return ($deletedCount === 2);
    }

    private function updateState($userId, $conversationId, $newState) {
        DB::table('user_conversation_pivot')
            ->where([
                'user_id' => $userId,
                'conversation_id' => $conversationId
            ])
            ->update(['state'=>$newState]);
    }

}