<?php

namespace App\Factories;

use App\Attachment;
use App\Conversation;
use App\Message;
use Exception;
use Illuminate\Support\Facades\DB;

class ConversationFactory {

    private $initiator;
    private $counterPart;
    private $messageFactory;

    private function __construct()
    {
        $this->messageFactory = MessageFactory::make();
    }

    public static function make()
    {
        return new self();
    }

    public function save()
    {
        try
        {
            DB::beginTransaction();
            $conversation = $this->persist();
            DB::commit();

            return $conversation;
        } catch (Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
    }

    private function persist()
    {
        $conversation = Conversation::createBetween($this->initiator, $this->counterPart);

        $this->messageFactory->setConversation($conversation->id);
        $this->messageFactory->save();

        return $conversation;
    }

    // Fluent trick to execute a method only when a specific condition is satisfied
    public function when(bool $condition, \Closure $callback)
    {
        if ($condition)
        {
            $callback($this);
        }

        return $this;
    }

    //Setters
    public function setInitiator($initiator)
    {
        $this->initiator = $initiator;
        $this->messageFactory->setSender($initiator);

        return $this;
    }

    public function setCounterPart($counterPart)
    {
        $this->counterPart = $counterPart;
        $this->messageFactory->setRecipient($counterPart);

        return $this;
    }

    public function setMessage($message)
    {
        $this->messageFactory->setBody($message);

        return $this;
    }

    public function setAttachments(array $attachments)
    {
        $this->messageFactory->setAttachments($attachments);

        return $this;
    }

}