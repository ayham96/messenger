<?php


namespace App\Factories;


use App\Attachment;
use App\Message;
use Illuminate\Validation\ValidationException;

class MessageFactory {

    private $sender;
    private $recipient;
    private $conversation;
    private $body;
    private $attachments = [];

    private function __construct()
    {
    }

    public static function make()
    {
        return new self();
    }


    public function save()
    {
        $this->validate();
        $message = $this->persist();

        return $message;
    }

    private function persist()
    {
        $message = Message::create([
            'sender_id'       => $this->sender,
            'recipient_id'    => $this->recipient,
            'conversation_id' => $this->conversation,
            'body'            => $this->body,
        ]);

        if (count($this->attachments) > 0)
        {
            $attachmentsModels = [];
            foreach ($this->attachments as $attachment)
            {
                $path = $attachment->store('attachments/' . $this->conversation);
                $attachmentsModels[] = new Attachment([
                    'path'              => $path,
                    'original_filename' => $attachment->getClientOriginalName(),
                ]);
            }
            $message->attachments()->saveMany($attachmentsModels);
        }

        return $message;
    }

    // Fluent trick to execute a method only when a specific condition is satisfied
    public function when(bool $condition, \Closure $callback)
    {
        if ($condition)
        {
            $callback($this);
        }

        return $this;
    }

    //Setters
    public function setSender($sender)
    {
        $this->sender = $sender;

        return $this;
    }

    public function setRecipient($recipient)
    {
        $this->recipient = $recipient;

        return $this;
    }


    public function setBody($body)
    {
        $this->body = $body;

        return $this;
    }

    public function setAttachments(array $attachments)
    {
        $this->attachments = $attachments;

        return $this;
    }

    public function setConversation($conversation)
    {
        $this->conversation = $conversation;

        return $this;
    }

    private function validate()
    {
        if ($this->sender === $this->recipient) {
            throw new \Exception("Validation exception sender and recipient can't be the same");
        }
    }
}