<?php

namespace App\Listeners;

use App\CacheSystem\ConversationCache;
use App\Events\MessageSaved;
use App\Managers\ConversationManager;
use App\Repositories\RecentContactsRepository;


class MessageSavedListener
{
    private $conversationManager;
    private $conversationCache;
    private $recentContactsRepository;

    public function __construct(ConversationManager $conversationManager, ConversationCache $conversationCache, RecentContactsRepository $recentContactsRepository)
    {
        $this->conversationManager = $conversationManager;
        $this->conversationCache = $conversationCache;
        $this->recentContactsRepository = $recentContactsRepository;
    }

    /**
     * Handle the event.
     *
     * @param  MessageSaved  $event
     * @return void
     */
    public function handle(MessageSaved $event)
    {
        $message = $event->message;
        $this->conversationManager->updateLastMessage($message->conversation_id, $message->id);
        $this->conversationManager->moveToInbox($message->sender_id, $message->conversation_id);
        $this->conversationManager->moveToInbox($message->recipient_id, $message->conversation_id);

        //Put in recent contacts
        $this->recentContactsRepository->put($event->message->recipient);

        //Clear the cache
        $this->conversationCache->messageCreated($message);

        //Todo implement a realtime notification system
    }
}
