<?php

namespace App\Listeners;

use App\CacheSystem\ConversationCache;
use App\Events\ConversationTrashed;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class ConversationTrashedListener
{
    private $conversationCache;

    public function __construct(ConversationCache $conversationCache)
    {
        $this->conversationCache = $conversationCache;
    }

    public function handle(ConversationTrashed $event)
    {
        //clear cache
        $this->conversationCache->conversationTrashed($event->userId);
    }
}
