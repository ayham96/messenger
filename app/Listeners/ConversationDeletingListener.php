<?php

namespace App\Listeners;

use App\CacheSystem\ConversationCache;
use App\Events\ConversationDeleting;
use Illuminate\Support\Facades\Storage;

class ConversationDeletingListener {

    private $conversationCache;

    public function __construct(ConversationCache $conversationCache)
    {
        $this->conversationCache = $conversationCache;
    }

    public function handle(ConversationDeleting $event)
    {
        //Related models in the database would automatically be deleted (ON DELETE CASCADE)

        //Delete attachments directory for the conversation:
        $attachmentsDirectory = 'attachments/' . $event->conversation->id;
        Storage::deleteDirectory($attachmentsDirectory);

        //Clear cache
        $this->conversationCache->conversationDeleted($event->conversation);
    }
}
