<?php

namespace App\Listeners;

use App\CacheSystem\ConversationCache;
use App\Events\ConversationArchived;

class ConversationArchivedListener
{
    private $conversationCache;

    public function __construct(ConversationCache $conversationCache)
    {
        $this->conversationCache = $conversationCache;
    }

    public function handle(ConversationArchived $event)
    {
        $this->conversationCache->conversationArchived($event->userId);
    }
}
