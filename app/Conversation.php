<?php

namespace App;

use App\Events\ConversationDeleting;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Conversation extends Model {

    protected $fillable = ['initiator_id', 'counter_part_id'];

    protected $dispatchesEvents = [
        'deleting' => ConversationDeleting::class,
    ];

    public static function createBetween($initiator_id, $counter_part_id)
    {
        $conversation = self::create([
            'initiator_id'    => $initiator_id,
            'counter_part_id' => $counter_part_id,
        ]);
        DB::table('user_conversation_pivot')->insert([
            ['conversation_id' => $conversation->id, 'user_id' => $initiator_id],
            ['conversation_id' => $conversation->id, 'user_id' => $counter_part_id],
        ]);

        return $conversation;
    }

    public function updateLastMessageId($message_id)
    {
        $this->last_message_id = $message_id;
        $this->save();
    }

    //Relationships
    public function lastMessage()
    {
        return $this->belongsTo(Message::class, 'last_message_id');
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    public function counterPart()
    {
        return $this->belongsToMany(User::class, 'user_conversation_pivot')
            ->where('user_id', '<>', auth()->id());
    }

}
