<?php

namespace App\Repositories;

use App\CacheSystem\ConversationCache;
use App\Message;
use Illuminate\Support\Facades\DB;

class ConversationRepository {

    const PER_PAGE = 25;

    private $conversationCache;

    public function __construct(ConversationCache $conversationCache)
    {
        $this->conversationCache = $conversationCache;
    }

    public function inbox($userId, $page = 1)
    {
        //Get directly from the database if it's not the first page (Only first page is cached)
        if ($page !== 1)
        {
            return $this->getConversations($userId, 'inbox', $page);
        }

        //Return cached
        if ($inbox = $this->conversationCache->getInbox($userId))
        {
            return $inbox;
        }

        //Get and save in cache
        $inbox = $this->getConversations($userId, 'inbox');
        $this->conversationCache->storeInbox($userId, $inbox);

        return $inbox;
    }

    public function archive($userId, $page = 1)
    {
        return $this->getConversations($userId, 'archived', $page);
    }

    public function messagesFor($conversationId)
    {
        if ($messages = $this->conversationCache->getMessages($conversationId))
        {
            return $messages;
        }

        $messages = Message::with('attachments')
            ->where('conversation_id', $conversationId)
            ->get();
        $this->conversationCache->storeMessages($conversationId, $messages);

        return $messages;
    }

    private function getConversations($userId, $state, $page = 1)
    {
        $paginationBindings = [
            'skip' => self::PER_PAGE * ($page - 1),
            'take' => self::PER_PAGE,
        ];
        $results = DB::select(DB::raw("
                SELECT 
                conversations.id AS id,
                conversations.updated_at AS timestamp,
                users.name AS counter_part_name,
                users.avatar_path AS counter_part_avatar_path,
                messages.body AS last_message_body,
                messages.sender_id AS last_message_sender_id
                FROM conversations 
                JOIN (user_conversation_pivot) 
                ON (id = conversation_id AND state = '$state' AND user_id = $userId )
                JOIN users
                ON (users.id = if(initiator_id = $userId , counter_part_id, initiator_id))
                JOIN messages
                ON (conversations.last_message_id = messages.id)
                ORDER BY timestamp DESC 
                LIMIT :skip , :take
        "), $paginationBindings);

        return collect($results);
    }
}