<?php

namespace App\Repositories;

use App\User;
use Illuminate\Support\Facades\Redis;

// todo: invalidate cache when profile is changed
class RecentContactsRepository {
    private $userId;
    private $cacheKey;
    const CACHE_COUNT = 5;

    public function __construct($userId)
    {
        $this->userId = $userId;
        $this->cacheKey = "user:$userId:recent_contacts";
    }

    public function get() {
        $recentContacts = Redis::zrevrange($this->cacheKey, 0, self::CACHE_COUNT);
        $decoded =  collect($recentContacts)->map(function($contact) {
            return json_decode($contact);
        });
        return $decoded;
    }

    public function put(User $contact) {
        Redis::zadd($this->cacheKey, time(), $contact);
        //trim
        Redis::zremrangebyrank($this->cacheKey, 0, 0-self::CACHE_COUNT);
    }
}