<?php

namespace App\Events;

use App\Message;
use Illuminate\Foundation\Events\Dispatchable;

class MessageSaved
{
    use Dispatchable;

    public $message;

    public function __construct(Message $message)
    {
        $this->message = $message;
    }
}
