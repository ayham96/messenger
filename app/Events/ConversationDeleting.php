<?php

namespace App\Events;

use App\Conversation;
use Illuminate\Foundation\Events\Dispatchable;

class ConversationDeleting
{
    use Dispatchable;

    public $conversation;

    public function __construct(Conversation $conversation)
    {
        $this->conversation = $conversation;
    }

}
