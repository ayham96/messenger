<?php

namespace App\Events;

use Illuminate\Foundation\Events\Dispatchable;

class ConversationTrashed
{
    use Dispatchable;

    public $userId;
    public $conversationId;

    public function __construct($userId, $conversationId)
    {
        $this->userId = $userId;
        $this->conversationId = $conversationId;
    }

}
