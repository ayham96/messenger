<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {

    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'avatar_path',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function search($searchQuery)
    {
        $results = self::where('id', '<>', auth()->id())
            ->where(function ($query) use ($searchQuery) {
                $query->where('name', 'like', $searchQuery . '%')
                    ->orWhere('email', 'like', $searchQuery . '%');
            })
            ->limit(5)
            ->get();

        return $results;
    }

    // Relationships
    public function conversations()
    {
        return $this->belongsToMany(Conversation::class, 'user_conversation_pivot')
            ->withTimestamps()
            ->withPivot('state');
    }

    // Scopes
    public function scopeExcept($query, $id)
    {
        return $query->where('id', '<>', $id);
    }
}
