<?php

namespace App;

use App\Events\MessageSaved;
use Illuminate\Database\Eloquent\Model;

class Message extends Model {

    protected $fillable = ['sender_id', 'recipient_id', 'body', 'conversation_id'];

    protected $dispatchesEvents = [
        'saved' => MessageSaved::class,
    ];

    public function isPart($userId) {
        return ($this->sender_id === $userId || $this->recipient_id === $userId);
    }

    //Relationships
    public function conversation()
    {
        return $this->belongsTo(Conversation::class);
    }

    public function sender()
    {
        return $this->belongsTo(User::class, 'sender_id');
    }

    public function recipient()
    {
        return $this->belongsTo(User::class, 'recipient_id');
    }

    public function attachments()
    {
        return $this->hasMany(Attachment::class);
    }

}
