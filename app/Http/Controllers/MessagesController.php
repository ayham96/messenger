<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMessageRequest;
use App\Factories\ConversationFactory;
use App\Factories\MessageFactory;
use App\User;
use Illuminate\Http\Request;

class MessagesController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function create(Request $request)
    {
        $inputData = $request->validate(['recipient_id' => 'integer']);
        $recipient = ($request->has('recipient_id')) ?
            User::findOrFail($inputData['recipient_id'])
            : null;

        return view('messages.create', compact('recipient'));
    }

    public function store(StoreMessageRequest $request)
    {
        $inputData = $request->all();
        if (empty($inputData['conversation_id']))
        {
            ConversationFactory::make()
                ->setInitiator(auth()->id())
                ->setCounterPart($inputData['recipient_id'])
                ->setMessage($inputData['message'])
                ->when($request->has('attachments'), function ($factory) use ($request) {
                    $factory->setAttachments($request->file('attachments'));
                })
                ->save();
        } else
        {
            MessageFactory::make()
                ->setSender(auth()->id())
                ->setRecipient($inputData['recipient_id'])
                ->setConversation($inputData['conversation_id'])
                ->setBody($inputData['message'])
                ->when($request->has('attachments'), function ($factory) use ($request) {
                    $factory->setAttachments($request->file('attachments'));
                })
                ->save();
        }

        return redirect('/inbox');
    }
}
