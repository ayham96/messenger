<?php

namespace App\Http\Controllers;

use App\Managers\ConversationManager;
use Illuminate\Http\Request;

class ArchiveController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index(Request $request, ConversationManager $conversationManager)
    {
        $page = $request->input('page', 1);
        $conversations = $conversationManager->getarchived();

        return view('archive', compact('conversations','page'));
    }

    /**
     * Store conversation in archvie
     * @param Request $request
     * @param ConversationManager $conversationManager
     * @return string
     */
    public function store(Request $request, ConversationManager $conversationManager)
    {
        $inputData = $request->validate([
            'conversation_id' => 'required|exists:conversations,id',
        ]);
        $conversationManager->archive(auth()->id(), $inputData['conversation_id']);

        return 'success';
    }
}
