<?php

namespace App\Http\Controllers;

use App\Repositories\RecentContactsRepository;
use App\User;

class ContactsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(RecentContactsRepository $recentRepository)
    {
        $recentContacts = $recentRepository->get();
        $contacts = User::except(auth()->id())->orderBy('name')->paginate(10);

        return view('contacts.index', compact('contacts', 'recentContacts'));
    }
}
