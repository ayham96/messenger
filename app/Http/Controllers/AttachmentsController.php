<?php

namespace App\Http\Controllers;

use App\Attachment;

class AttachmentsController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Attachment $attachment)
    {
        $isPart = $attachment->message->isPart(auth()->id());
        if ($isPart)
        {
            $absolutePath = storage_path('app/' . $attachment->path);

            return response()->download($absolutePath, $attachment->original_filename);
        } else
        {
            return redirect('/');
        }
    }
}
