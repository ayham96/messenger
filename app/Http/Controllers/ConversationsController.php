<?php

namespace App\Http\Controllers;

use App\Managers\ConversationManager;

class ConversationsController extends Controller {

    private $conversationManager;

    public function __construct(ConversationManager $conversationManager)
    {
        $this->middleware('auth');
        $this->conversationManager = $conversationManager;
    }

    public function show(int $conversationId)
    {
        //Check if the authenticated user is a part in the conversation
        $user = auth()->user();
        $isPart = $this->conversationManager->isPart($user->id, $conversationId);
        if (!$isPart)
        {
            abort(401, 'Unauthorised');
        }
        $counterPart = $this->conversationManager->getCounterPart($conversationId, $user->id);
        $messages = $this->conversationManager->getMessages($conversationId);

        return view('conversations.show', compact('conversationId', 'messages', 'counterPart'));
    }

    public function destroy($conversationId, ConversationManager $conversationManager)
    {
        $conversationManager->delete(auth()->id(), $conversationId);

        return 'success';
    }
}
