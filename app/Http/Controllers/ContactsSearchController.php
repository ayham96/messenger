<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class ContactsSearchController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request)
    {
        $results = User::search($request->input('query'));

        return $results;
    }
}
