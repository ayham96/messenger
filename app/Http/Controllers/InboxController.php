<?php

namespace App\Http\Controllers;

use App\Conversation;
use App\Managers\ConversationManager;
use Illuminate\Http\Request;

class InboxController extends Controller {

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function __invoke(Request $request,ConversationManager $conversationManager)
    {
        $page = $request->input('page', 1);
        $conversations = $conversationManager->getInbox($page);

        return view('inbox', compact('conversations', 'page'));
    }
}
