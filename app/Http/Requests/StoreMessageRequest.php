<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreMessageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //If the message is a reply to a conversation check if the user is a part in that conversation.
        //$user = auth()->user();
        //$user->conversations()->where('id','');
        if ((int) $this->input('recipient_id') === auth()->id()) {
            return false;
        }
        if ($this->has('conversation_id')) {
            //todo completehas
            //check if is part in conversation
            $conversation_id = $this->input('conversation_id');
        }
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules =  [
            'recipient_id'=>'required|exists:users,id',
            'message'=>'required|max:255',
            'conversation_id' => 'exists:conversations,id',
            'attachments.*' => 'file|mimes:jpeg,jpg,png,doc,docx,txt,md|max:2000',
        ];
        return $rules;
    }
}
