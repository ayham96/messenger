<?php

namespace App\CacheSystem;


use App\Conversation;
use App\Message;
use Illuminate\Support\Facades\Cache;

class ConversationCache {

    const CACHE_PERIOD = 86400; //In minutes

    public function getMessages($conversationId)
    {
        return Cache::get("messages:$conversationId");
    }

    public function getInbox($userId)
    {
        return Cache::get("user:$userId:inbox");
    }

    public function storeMessages($conversationId, $messages)
    {
        Cache::put("messages:$conversationId", $messages, self::CACHE_PERIOD);
    }

    public function storeInbox($userId, $inbox)
    {
        Cache::put("user:$userId:inbox", $inbox, self::CACHE_PERIOD);
    }

    /*
     * Clears the effected cache values when a new message is sent
     */
    public function messageCreated(Message $message)
    {
        $this->clearMessages($message->conversation_id);
        $this->clearInbox($message->sender_id);
        $this->clearInbox($message->recipient_id);
    }

    /*
     * Clears the effected cache values when a conversation is archived by a specific user.
     */
    public function conversationArchived($userId)
    {
        $this->clearInbox($userId);
    }

    /*
     * Clears the effected cache values when a conversation is deleted by both users.
     */
    public function conversationDeleted(Conversation $conversation)
    {
        $this->clearMessages($conversation);
    }

    /*
     * Clears the effected cache values when a conversation is deleted by one user.
     */
    public function conversationTrashed($userId)
    {
        $this->clearInbox($userId);
    }


    private function clearInbox($userId)
    {
        Cache::forget("user:$userId:inbox");
    }

    private function clearMessages($conversationId)
    {
        Cache::forget("messages:$conversationId");
    }
}