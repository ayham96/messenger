<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Attachment extends Model {

    protected $fillable = ['message_id', 'path', 'original_filename'];

    public $timestamps = false;

    public function message()
    {
        return $this->belongsTo(Message::class);
    }

    public function toArray()
    {
        return [
            'id'       => $this->id,
            'filename' => $this->original_filename,
        ];
    }
}
